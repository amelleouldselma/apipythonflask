from flask import Flask, escape, request
from flask import render_template
from flask_pymongo import PyMongo



app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/myDatabase"
mongo = PyMongo(app)

@app.route('/')
def accueil():
    return render_template('accueil.html')
    name = request.args.get("name", "World")
    return f'Hello, {escape(name)}!'


@app.route('/api/v1/')
def documentation():
    return render_template('documentation.html')


@app.route('/api/v1/topics')
def liste():
    return render_template('liste.html')

"""
@api {get} /user/:id Request User information
@apiName GetUser
@apiGroup User

@apiParam {Number} id Users unique ID.

@apiSuccess {String} firstname Firstname of the User.
@apiSuccess {String} lastname  Lastname of the User.
""" 
@app.route('/api/v1/topics{Id}')
def dashboard():
    return render_template('dashboard.html')

@app.route('/apidoc')
def apidoc():
    return render_template('apidoc/index.html')